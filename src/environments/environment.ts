// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firestore:{
    apiKey: "AIzaSyAf_bDYWYm4CiFMIr43PLkh8wvSMIyFjEA",
    authDomain: "app-control-clientes.firebaseapp.com",
    projectId: "app-control-clientes",
    storageBucket: "app-control-clientes.appspot.com",
    messagingSenderId: "633774602140",
    appId: "1:633774602140:web:e1a729c7e8e764e34c3a59"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
